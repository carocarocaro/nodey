import {getUsers, updateUser} from './usersDao';

const express = require('express');
const app = express();
const port = 3000;

app.get('/get-users', (req, res) => {
  getUsers()
    .then(users => {
      res.send(users);
    });
});

app.get('/hello-world', (req, res) => {
  res.send('Hello World');
});

app.get('/nodey-frontend-javascript', (request, response) => {
  response.set('Content-Type', 'application/javascript');
  response.send(`
    $.get( "http://localhost:3000/get-users", function( data ) {
      console.log('HEY!', data);
      $( ".result" ).html( JSON.stringify(data)  )
    });
  `);
});

app.set('view engine', 'pug');
app.set('views', './views');

app.get('/', (request, response) => {
  // get the pug file as a string
  // run the file thorugh the pug library
  // return whatever the pug library returns
  response.render('main-page');
});

app.listen(port, (err) => {
  if (err) {
    return console.log('Something went wrong', err)
  }

  console.log(`The server is listening on ${port}`)
});