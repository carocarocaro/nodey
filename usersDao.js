
const knex = require('knex')({
  client: 'pg',
  connection: {
    host : 'localhost',
    user : 'austinbrown',
    password : '',
    port: 5432,
    database : 'noteninja'
  }
});


export function getUsers() {
  return knex.raw(`
      select * 
      from main.users u
      join main.user_organization_settings uos on uos.user_id = u.id
    `)
    .then(results => results.rows);
}

export function updateUser(userId, newNameToSave) {
  return knex.raw(`
    update main.users
    set first_name = :name 
    where id = :id
    returning *
  `, { id: userId, name: newNameToSave })
  .then(results => results.rows);
}